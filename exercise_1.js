const counter = (function () {
    var counter = 0;
    return function(){
        counter += 1; return counter
    }
})();

console.log(counter());
console.log(counter()); 
console.log(counter());

/**
 * Lo que hace la funcion es establecer un clousure, la cual te permite entrar en el ambito de una funcion exterior desde una interior
 * Esto se podria usar para generar variables privadas
 */