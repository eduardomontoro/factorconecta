var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' })
})

router.post('/palindrome', function(req, res) {
  const pharase = req.body.phrase.replace(/\s+/g, '');
  
  let pharaseArray = Array.from(pharase)

  let reversed = pharaseArray.reverse()

  reversed = reversed.join('')
  
  
  palindrome  = reversed == pharase ? true : false

  res.json({'palindrome': palindrome})

})

module.exports = router
